﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardidMastidEnummid
{
    // What is an enum?

    // enum Mast { Clubs, Diamonds, Hearts, Spades }

    // An enum is a number datatype, which is used for counting, rather than summing. 
    // an enum essentially is a list of numbers which have been given/assigned names. 
    // The order can be manipulated as well;
    enum Suit { Clubs = 0, Diamonds, Hearts, Spades };
    enum Card { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace }


    class Program
    {
        static void Main(string[] args)
        {
            Suit m = Suit.Clubs;



            Random r = new Random();


            

            Dictionary<int, string> deck = new Dictionary<int, string>();
            List<int> randomNumbers = new List<int>();

            // Can you have a dictionary with a key of string and a value of int array?

            Dictionary<string, int[]> deckTwo = new Dictionary<string, int[]>();

            // Ülesande keerukus
            // Kui me räägime nt kokkuliitmisest, siis 3 arvu kokku liitmine ja 30 000 arvu kokku liitmine võtab erinev hulk aega. 
            // See ülesanne on siiski O astmes 1 ehk lineaarse keerukusega.
            // Kui me räägime nt sorteerimisest, on keerukus O astmes 2 ehk ruutvõrdeline.
            // On ülesandeid, mis on ka eksponentsiaalse keerukusega. 



            // Ma teen dictionary, kus v6ti on kaardi "nimi", string, ja selle v22rtus on selle v22rtus, int. 
            Dictionary<string, int> deckWithValues = new Dictionary<string, int>();

            int numberOfCardsPerSuit = 13;
            int minimumValue = 2;
            int deckSize = 52;

            while (randomNumbers.Count < deckSize)
            {
                int random = r.Next(0, deckSize);
                if (!randomNumbers.Contains(random))
                {
                    randomNumbers.Add(random);
                }
                
            }
            
            for (int i = 0; i < 52; i++)
            {

                //int cardNumber = randomNumbers[i];
                int cardNumber = i;
                Suit suit = (Suit)(cardNumber / numberOfCardsPerSuit);
                Card card = (Card)(cardNumber % numberOfCardsPerSuit + minimumValue);

                

                string Suit = suit.ToString();
                string Card = card.ToString();
                string fullCardName = Suit + " " + Card;
                

                deckWithValues.Add(fullCardName, (int)card);

                deck.Add(i, fullCardName);
               
            }


            for (int i = 1; i < deck.Count + 1; i++)
            {
                Console.Write($"\t {deck[i - 1]}");
                if (i % 4 == 0)
                {
                    Console.WriteLine('\n');
                }
            }


            //foreach (string key in deckWithValues.Keys)
            //{
            //    Console.Write(key);
            //    Console.Write(" ");
            //    Console.Write(deckWithValues[key]);
            //    Console.WriteLine();

            //}
            //foreach (KeyValuePair<int, string> item in deck)
            //{
            //    Console.WriteLine(item.Value);
            //}
            //Console.WriteLine(deck.Count);


            // TO DO!!
            // Print in 4 columns
            // Sort each column by value
            // 
        }
    }
}
